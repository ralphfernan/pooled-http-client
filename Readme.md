##Execute a number of concurrent Http connections to an origin server with delay.

###Build and run on Mac, without docker:

	mvn clean package && \pushd target && java -jar pooled-rest-client-0.1.0.jar

###Then build & run with Docker (optional):
```
	docker build -t pooled-http-client .
	docker run -p 8080:8080 pooled-http-client
```
  
###Then make concurrent requests to Montebank server (with configured delay):
```
http://localhost:8080/?threads=800
```
Monitor connections on Mac with:

```
lsof -i | grep -E "qa-bp-private-i-024c8be9f17c43996.opsecol.net"
```

###Example output:
```
	[{
	"headers": {
		"Connection": ["close"],
		"Date": ["Fri, 21 Aug 2020 21:01:50 GMT"],
		"Transfer-Encoding": ["chunked"]
	},
	"body": "This took at least 1.5 second to send",
	"statusCode": "OK",
	"statusCodeValue": 200
	...
```
Notes:	
By default, RestTemplate uses Java HttpUrlConnection (not a pool).
Here, RestTemplate is using Apache HttpComponents HttpClient (not a pool either).
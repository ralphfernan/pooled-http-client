package ralph;
import java.net.URI;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@RestController
public class Application {

  @RequestMapping("/")
  public List<ResponseEntity<String>> home(@Qualifier("pooledRestTemplate") RestTemplate restTemplate
		  , @RequestParam(name = "threads", required = true) int nThreads) throws InterruptedException {

	URI uri = URI.create("http://qa-bp-private-i-024c8be9f17c43996.opsecol.net:31804");

	Callable<ResponseEntity<String>> callable = () -> {
		ResponseEntity<String> response = restTemplate.getForEntity(uri, String.class);	
		return response;
	};
	
	List<Callable<ResponseEntity<String>>> tasks = new ArrayList<>(nThreads);
	for (int i = 0; i<nThreads; i++) {
		tasks.add(callable);
	}
	
	ExecutorService svc = Executors.newFixedThreadPool(nThreads);
	List<Future<ResponseEntity<String>>> futures = svc.invokeAll(tasks);
	List<ResponseEntity<String>> results = futures.stream().map(el->{
		ResponseEntity<String> result = null;
		try {
			result = el.get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		} 
		return result;
	}).collect(Collectors.toList());
    return results;
  }

  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }

}
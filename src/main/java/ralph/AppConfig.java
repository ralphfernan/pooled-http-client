package ralph;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

@Configuration
public class AppConfig {
	private final Log log = LogFactory.getLog(getClass());
	
    @Bean(name = "pooledRestTemplate")
    RestTemplate getPooledRestTemplate(@Autowired RestTemplateBuilder restTemplateBuilder) {
    	log.info("Creating pooledRestTemplate");
        String urlboxRootUri = "http://qa-bp-private-i-024c8be9f17c43996.opsecol.net:31804"; // Montebank in QA with 1.5s delay
		final RestTemplate restTemplate = restTemplateBuilder.rootUri(urlboxRootUri).build();
        restTemplate.setRequestFactory(getClientHttpRequestFactory());
        return restTemplate;
    }
    
    private ClientHttpRequestFactory getClientHttpRequestFactory() {
    	
    	// Use HttpComponentsClientHttpRequestFactory(org.apache.http.client.HttpClient httpClient) for a Pooled Client
    	// defaults to CloseableHttpClient
        final HttpComponentsClientHttpRequestFactory clientHttpRequestFactory =
                new HttpComponentsClientHttpRequestFactory(); 

        clientHttpRequestFactory.setConnectTimeout(1000); // in milliseconds

        clientHttpRequestFactory.setConnectionRequestTimeout(1000);

        clientHttpRequestFactory.setReadTimeout(1000);

        return clientHttpRequestFactory;
    }

}
